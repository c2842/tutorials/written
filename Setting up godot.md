# Overview
For the purposes of this I am going to be using the C# version of godot.  
I am recommending to use the C#/Mono version as it allows the use of both GDScript and C#.  
GDScript is close enough to Lua and Python that it should be pretty easy to pick up.  
C# (C Sharp) is a language created by Microsoft and has uses outside of Godot, and thus good for yer Co-op CV/longterm.

I am going to be using it with the Intellij IDE called Rider which is specifically for C#.  
While these  instructions are based off windows they shpould still be roughly applicable for linux and mac.  
If you have a custom setup you aught to have the skills to adapt the instructions

# Instructions

1. Download DotNet (.Net) dependencies
   * https://dotnet.microsoft.com/en-us/download/dotnet
   * Download both 6.0 and Core
   * Make sure you get the x64 versions, not the ARM ones
   * Make sure you download the installer and not the binaries.
2. Install Rider
   * Prerequisite
      * A license for the full suite Jetbrains (Intellij) IDEs is required for Rider. (It is free if you are a student)
         * https://www.jetbrains.com/community/education/#students
   1. Install it from teh Jetbrains (Intellij) Toolbox
   2. if you dont have the toolbox install it and go to 2.1 
      * https://www.jetbrains.com/toolbox-app/
   3. A popup will come up when you just after the install is complete, leave it at default valuse and press continue.
   4. If you have VSCode already install, I would advise against importing the settings.
   5. Follow the instructions for the themes (select the ones that you would prefer, they can be changed in settings later)
      * For keyboard shortcuts, I would advise the Intellij preset.
   6. Install the Godot and GDScript plugins.
3. Download Godot Mono
   * https://godotengine.org/download/windows
   * This does not install but rather is a standalone binary, I recommend right clicking and adding to start menu.
4. Follow the final config settings here
   * https://docs.godotengine.org/en/stable/tutorials/scripting/c_sharp/c_sharp_basics.html#doc-c-sharp-setup
5. Complete
   * I recommend checking out the 2D tutorial https://docs.godotengine.org/en/stable/getting_started/first_2d_game/index.html

# N.B.
* Everytime that you connrct a script godot will give you `_on_MobTimer_timeout`, but if you are using C# it should be `OnMobTimerTimeout`

# Other Links
* Godot Discord Server
   * https://discord.gg/gBC5xy3AdY
